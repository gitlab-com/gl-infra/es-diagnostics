#!/usr/bin/env bash

# Requires the following env vars:
# CLUSTER_URL
# CLUSTER_CREDS
# BUCKET_NAME

# references for interpreting results
# https://github.com/elastic/elasticsearch/blob/master/server/src/main/java/org/elasticsearch/monitor/jvm/HotThreads.java
# https://docs.oracle.com/javase/7/docs/api/java/lang/management/ThreadInfo.html
# https://docs.oracle.com/javase/7/docs/api/java/lang/Thread.State.html

set -euo pipefail

# additional settings for the hot threads api
THREADS_INTERVAL="${THREADS_INTERVAL:-500ms}"
THREADS_SNAPSHOTS="${THREADS_SNAPSHOTS:-10}"
THREADS_THREADS="${THREADS_THREADS:-3}"

CONT_HEADER="Content-Type: application/json"
DIR_NAME="es_threads_tasks_dump_$(date +%Y-%m-%d_%H:%M:%S -u)"
SCRIPT_DIR="$(dirname "$0")"

if [[ -z ${CLUSTER_URL+x} ]] || [[ -z ${CLUSTER_CREDS+x} ]] || [[ -z ${BUCKET_NAME+x} ]]; then
  echo "Required env vars missing. Please see the script for more details."
  exit 1
fi


mkdir "${DIR_NAME}"
curl -f -H "${CONT_HEADER}" -u "${CLUSTER_CREDS}" "${CLUSTER_URL}_cat/indices?v&pri&s=index&h=index,pri,rep,docs.count,docs.deleted,pri.store.size&bytes=gb" >"${DIR_NAME}/cat_indices"
curl -f -H "${CONT_HEADER}" -u "${CLUSTER_CREDS}" "${CLUSTER_URL}_cat/thread_pool?v" >"${DIR_NAME}/cat_thread_pool"
curl -f -H "${CONT_HEADER}" -u "${CLUSTER_CREDS}" "${CLUSTER_URL}_cluster/health" >"${DIR_NAME}/cluster_health.json"
curl -f -H "${CONT_HEADER}" -u "${CLUSTER_CREDS}" "${CLUSTER_URL}_cluster/pending_tasks" >"${DIR_NAME}/cluster_pending_tasks.json"
curl -f -H "${CONT_HEADER}" -u "${CLUSTER_CREDS}" "${CLUSTER_URL}_tasks?detailed=true" >"${DIR_NAME}/tasks.json"
curl -f -H "${CONT_HEADER}" -u "${CLUSTER_CREDS}" "${CLUSTER_URL}_nodes/hot_threads?interval=${THREADS_INTERVAL}&snapshots=${THREADS_SNAPSHOTS}&threads=${THREADS_THREADS}&type=cpu" >"${DIR_NAME}/hot_threads_cpu"
curl -f -H "${CONT_HEADER}" -u "${CLUSTER_CREDS}" "${CLUSTER_URL}_nodes/hot_threads?interval=${THREADS_INTERVAL}&snapshots=${THREADS_SNAPSHOTS}&threads=${THREADS_THREADS}&type=wait" >"${DIR_NAME}/hot_threads_wait"
curl -f -H "${CONT_HEADER}" -u "${CLUSTER_CREDS}" "${CLUSTER_URL}_nodes/hot_threads?interval=${THREADS_INTERVAL}&snapshots=${THREADS_SNAPSHOTS}&threads=${THREADS_THREADS}&type=block" >"${DIR_NAME}/hot_threads_block"

handle_errors () {
  if [[ $1 -gt 0 ]]
  then
    echo "WARNING: The flamepgraph.pl script returned errors"
  fi
}

FLAMEGRAPH_EXIT_CODE=0
"${SCRIPT_DIR}/collapse_hot_threads.rb" --node --index <"${DIR_NAME}/hot_threads_cpu" | "${SCRIPT_DIR}/flamegraph.pl" --hash >"${DIR_NAME}/hot_threads_cpu.svg" || FLAMEGRAPH_EXIT_CODE=$?
handle_errors $FLAMEGRAPH_EXIT_CODE
"${SCRIPT_DIR}/collapse_hot_threads.rb" --node --index <"${DIR_NAME}/hot_threads_wait" | "${SCRIPT_DIR}/flamegraph.pl" --hash >"${DIR_NAME}/hot_threads_wait.svg" || FLAMEGRAPH_EXIT_CODE=$?
handle_errors $FLAMEGRAPH_EXIT_CODE
"${SCRIPT_DIR}/collapse_hot_threads.rb" --node --index <"${DIR_NAME}/hot_threads_block" | "${SCRIPT_DIR}/flamegraph.pl" --hash >"${DIR_NAME}/hot_threads_block.svg" || FLAMEGRAPH_EXIT_CODE=$?
handle_errors $FLAMEGRAPH_EXIT_CODE

# activate the service account if the env var is set
if [[ ! -z ${GOOGLE_APPLICATION_CREDENTIALS+x} ]]; then
  gcloud auth activate-service-account --key-file="${GOOGLE_APPLICATION_CREDENTIALS}"
fi

gsutil -m cp -R "${DIR_NAME}" "gs://${BUCKET_NAME}/"

rm -rf "${DIR_NAME}"
