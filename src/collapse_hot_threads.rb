#! /usr/bin/env ruby
# frozen_string_literal: true

# script for parsing output from elasticsearch's _nodes/hot_threads endpoint
# and collapsing the stacks to a format that can be piped into flamegraph.pl.
#
# example usage:
#  curl -s "$ELASTICSEARCH_URL/_nodes/hot_threads" > hot_threads
#  cat hot_threads | scripts/collapse_hot_threads.rb | scripts/flamegraph.pl > hot_threads.svg
#
# some notes on hot_threads behaviour and parameters:
#
#   the way the hot_threads endpoint captures stacks is not on-cpu stacks across
#   all threads as one might expect.
#
#   it takes two snapshots of "time on cpu" counters of all threads, 500ms apart
#   (this is the interval). then it picks the top k of those threads (interval
#   and k are configurable).
#
#   for those top k threads it collects n stack samples at 100hz (n is configurable).
#
#   you can configure type as "block", "cpu", or "wait". the default is "cpu".
#   this will only affect the scoring of which threads to sample.
#
#   the sampling of stacks is done via `ThreadMXBean.getThreadInfo()`. this
#   will sample regardless of whether that thread is on CPU or not.
#
# see also:
#   https://www.elastic.co/guide/en/elasticsearch/reference/current/cluster-nodes-hot-threads.html

require 'optparse'

options = {}
OptionParser.new do |opts|
  opts.banner = "Usage: collapse_hot_threads.rb [options]"

  opts.on('', '--node', 'include node name in stack') { |v| options[:node] = v }
  opts.on('', '--index', 'include index name in stack') { |v| options[:index] = v }
end.parse!

lines = ARGF.readlines.map(&:rstrip)

nodes = []

# ::: {instance-0000000025}{rczdV00SReGFkOT-sWcO-Q}{xR-MVKmoQ1-GHdKatAmN5Q}{instance-0000000025}{10.42.10.241}{10.42.10.241:19813}{hirst}{data=hot, server_name=instance-0000000025.bb91db01721b40ec844618a34ed67af3, instance_configuration=gcp.es.datahot.n2.68x10x45, region=unknown-region, availability_zone=us-central1-c, logical_availability_zone=zone-0, xpack.installed=true}

loop do
  break if lines.empty?

  l = lines.shift
  raise 'start with :::' unless l.start_with?(':::')
  raise unless /::: (\{.*\})+/.match(l)

  m = l.scan(/\{([^}]*)\}/)
  raise unless m

  # ["instance-0000000011", "bsyG_XFEQdqKMY59CxTPZw", "_1NZN0yuQ4SbQPmmkHlF8g", "instance-0000000011", "10.42.10.253", "10.42.10.253:19055", "hirst", "data=hot, server_name=instance-0000000011.bb91db01721b40ec844618a34ed67af3, instance_configuration=gcp.es.datahot.n2.68x10x45, region=unknown-region, availability_zone=us-central1-a, logical_availability_zone=zone-1, xpack.installed=true"]
  # ["tiebreaker-0000000064", "pndd6ESsStWKcbZ3QMxJng", "6BxcRIsKT8G7_CHsVXt7Cg", "tiebreaker-0000000064", "10.42.10.164", "10.42.10.164:19256", "mv", "8.7.1", "xpack.installed=true, data=hot, server_name=tiebreaker-0000000064.e8fabf53b73247e98df7c355d6a782fc, instance_configuration=gcp.master.1, region=unknown-region, availability_zone=us-central1-b, logical_availability_zone=tiebreaker"]

  matches = m.map(&:first)
  node_name, _node_id, _, _node_alias, _node_ip, _node_hostport, _node_role, _version, attrs = matches
  attrs = attrs.split(', ').map { |attr| attr.split('=', 2) }.to_h

  node = {
    name: node_name,
    attrs: attrs,
    threads: []
  }

  # Hot threads at 2023-05-03T15:55:02.854Z, interval=500ms, busiestThreads=3, ignoreIdleThreads=true:

  l = lines.shift
  raise 'hot threads' unless m = /^   Hot threads at (\S+), interval=(\S+), busiestThreads=(\S+), ignoreIdleThreads=(\S+):$/.match(l)

  timestamp, interval, busiest, ignore_idle = m.to_a[1..]

  node[:profile] = {
    timestamp: timestamp,
    interval: interval,
    busiest: busiest,
    ignore_idle: ignore_idle
  }

  # 0.0% (0s out of 500ms) block usage by thread 'elasticsearch[instance-0000000145][write][T#8]'
  # 100.0% [cpu=74.4%, other=25.6%] (500ms out of 500ms) cpu usage by thread 'elasticsearch[instance-0000000161][[pubsub-rails-inf-gprd-009165][5]: Lucene Merge Thread #50]'
  # 0.6% [cpu=0.6%, idle=99.4%] (500ms out of 500ms) cpu usage by thread 'elasticsearch[instance-0000000066][transport_worker][T#2]'
  # see also: https://github.com/elastic/elasticsearch/pull/79392

  loop do
    l = lines.shift
    raise 'expected blank' unless l == ''

    break 2 if lines.empty?

    break if lines.first.start_with?(':::')

    l = lines.shift
    raise 'thread name' unless m = /^\s+\S+% \[cpu=(\S+)%, (?:other|idle)=\S+%\] \((\S+) out of (\S+)\) (\S+) usage by thread '([^']+)'$|^\s+(\S+)% \((\S+) out of (\S+)\) (\S+) usage by thread '([^']+)'$/.match(l)

    percent, time_est, time_total, _profile_type, thread_name = m.to_a[1..]

    # elasticsearch[instance-0000000047][[pubsub-rails-inf-gprd-002977][5]: Lucene Merge Thread #1130]
    # elasticsearch[instance-0000000056][write][T#5]
    # QuotaAwareFSTimer-0

    index_name = nil

    if m = /^elasticsearch\[(\S+)\]\[\[(\S+)\]\[(\S+)\]: (.+)\]$/.match(thread_name)
      index_name = m[2]
      thread_name = m[4]
    elsif m = /^elasticsearch\[(\S+)\]\[(\S+)\]\[(\S+)\]$/.match(thread_name)
      thread_name = m[2]
    end

    thread_name = 'Lucene Merge Thread' if /^Lucene Merge Thread #\d+$/.match(thread_name)

    thread = {
      percent: percent,
      time_est: time_est,
      time_total: time_total,
      name: thread_name,
      index_name: index_name,
      samples: []
    }

    #     3/100 snapshots sharing following 34 elements

    loop do
      break if lines.first == ''

      l = lines.shift
      if l == '     unique snapshot'
        count_seen = 1
        count_total = 1
      else
        raise 'snapshots' unless m = %r{^     (\S+)/(\S+) snapshots sharing following (\S+) elements$}.match(l)

        count_seen, count_total, _ = m.to_a[1..]
      end

      sample = {
        count_seen: count_seen,
        count_total: count_total,
        stack: []
      }

      #        app//org.elasticsearch.action.admin.indices.stats.CommonStats.add(CommonStats.java:373)

      loop do
        break unless lines.first.start_with?('      ')

        l = lines.shift

        sample[:stack] << l.strip
      end

      thread[:samples] << sample
    end

    node[:threads] << thread
  end

  nodes << node
end

raise 'non-consumed lines' unless lines.empty?

# pp nodes
# exit 1

nodes.each do |node|
  node[:threads].each do |thread|
    thread[:samples].each do |sample|
      stack = []
      stack << node[:name] if options[:node]
      stack << thread[:name]
      stack << thread[:index_name] if options[:index] && thread[:index_name]
      stack += sample[:stack].reject { |f| /(\$\$Lambda\$|^org\.elasticsearch\.xpack\.security\.|Listener\.java:|ListenableFuture\.java:|AbstractRunnable\.java:)/.match(f) }.reverse
      puts "#{stack.join(';')} #{sample[:count_seen]}"
    end
  end
end
