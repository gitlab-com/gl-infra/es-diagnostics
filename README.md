# es-diagnostics

This is a periodic job that runs [on kubernetes](https://ops.gitlab.net/gitlab-com/gl-infra/k8s-workloads/gitlab-helmfiles/-/tree/master/releases/es-diagnostics).

It collects diagnostic data from Elasticsearch that is not otherwise available via prometheus or the monitoring cluster.

This currently includes:

* A dump of indices from `_cat/indices`.
* A dump of thread pools from `_cat/thread_pool`.
* Cluster health from `_cluster/health`.
* Pending tasks from `_cluster/pending_tasks`.
* Running tasks from `_tasks`.
* Hot threads (CPU profile) from `_nodes/hot_threads`.

The CPU profile is also converted to a [flamegraph](https://github.com/brendangregg/FlameGraph) via `collapse_hot_threads.rb`.

The results from this job are stored in a GCS bucket, viewable under:

```
➜  ~ gsutil ls gs://gitlab-ops-es-diagnostics/
```

Example listing:

```
➜  ~ gsutil ls gs://gitlab-ops-es-diagnostics/es_threads_tasks_dump_2020-08-24_11:20:06/
gs://gitlab-ops-es-diagnostics/es_threads_tasks_dump_2020-08-24_11:20:06/cat_indices
gs://gitlab-ops-es-diagnostics/es_threads_tasks_dump_2020-08-24_11:20:06/cat_thread_pool
gs://gitlab-ops-es-diagnostics/es_threads_tasks_dump_2020-08-24_11:20:06/cluster_health.json
gs://gitlab-ops-es-diagnostics/es_threads_tasks_dump_2020-08-24_11:20:06/cluster_pending_tasks.json
gs://gitlab-ops-es-diagnostics/es_threads_tasks_dump_2020-08-24_11:20:06/hot_threads.svg
gs://gitlab-ops-es-diagnostics/es_threads_tasks_dump_2020-08-24_11:20:06/hot_threads_index.svg
gs://gitlab-ops-es-diagnostics/es_threads_tasks_dump_2020-08-24_11:20:06/hot_threads_node.svg
gs://gitlab-ops-es-diagnostics/es_threads_tasks_dump_2020-08-24_11:20:06/nodes_hot_threads
gs://gitlab-ops-es-diagnostics/es_threads_tasks_dump_2020-08-24_11:20:06/tasks.json
```
