FROM google/cloud-sdk:alpine
MAINTAINER "GitLab Production Team <ops-contact@gitlab.com>"
COPY ./src /es-diagnostics
RUN apk add --no-cache \
  curl \
  perl \
  ruby
CMD /es-diagnostics/es_diagnostics.sh
